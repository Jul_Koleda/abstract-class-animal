<?php

/**
 * Написать класс Cat, который наследуется от класcа Animal. Класс Animal имеет метод getName (name можно передать в конструктор). Класс Cat имеет метод meow (возвращает строку «Cat {catname} is sayig meow».
 */
abstract class animal

{
    public $eye;

    public $legs;

    public $name;

    public $voice;


    public function __construct($eye = 'green', $legs = 4, $name='', $voice='')
    {
        $this->eye   = $eye;
        $this->legs  = $legs;
        $this->name  = $name;
        $this->voice = $voice;
    }

    abstract function getVoice();

    abstract function getName();
}

Class cat extends animal
{

    public function getName($name = '')
    {
        echo "$name";
    }

    public function getVoice($voice = 'meow', $name = 'Garfield')
    {
        echo "Cat $name is saying $voice";
    }

}

$pussy = new Cat(null, null, '', '');
$pussy->getName();
$pussy->getVoice();